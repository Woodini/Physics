﻿using UnityEngine;

public class DetectPlayer : MonoBehaviour {
	public Material mat;
	public Color off = Color.red;
	public Color on = Color.yellow;

	void Start() {
		mat.color = off;
	}

	private void OnTriggerEnter(Collider other) {
		//Destroy (other.gameObject);
		mat.color = on;
	}

	private void OnTriggerExit(Collider other) {
		mat.color = off;
	}

}
