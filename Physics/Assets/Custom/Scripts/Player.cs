﻿using UnityEngine;

public class Player : MonoBehaviour {
	public float speed = 7.0f;
	private Vector2 input;
	private Rigidbody rb;

	void Start() {
		rb = GetComponent<Rigidbody> ();
	}

	void Update() {
		input = new Vector2 (Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")) * speed;
	}

	void FixedUpdate () {
		rb.velocity = new Vector3 (input.x, 0, input.y);
	}
}
